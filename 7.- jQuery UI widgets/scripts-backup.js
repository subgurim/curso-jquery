$(tooltipExamples);

function tooltipExamples(){
	$('p').tooltip();
}


function tabsExamples() {
	$( "#tabs" ).tabs();
}

// Download http://jquery-ui.googlecode.com/svn/tags/latest/ui/i18n/
function dateTimePickesExamples() {

	//$( "#datepicker" ).datepicker();
	$( "#datepicker" ).datepicker({
		numberOfMonths: 3,
		onSelect : function(date, inst){
			console.log(date);
		}
	});
	
	$( "#datepicker" ).datepicker( "option", $.datepicker.regional[ 'es' ] );
}