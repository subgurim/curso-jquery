$(function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
	     minDate: '-100Y',
	     yearRange: "1900:2015"
    });
  });