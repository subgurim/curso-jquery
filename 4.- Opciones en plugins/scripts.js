$(document).ready(function(){
	$('.font-sizer').fontSizer({ 
		target : '.fontsizer-target2',
		onFontResized : function(param1) { 
			console.log(this);
			console.log(param1);
		}
	});

	$.fn.hilight.defaults.foreground = "green";
	
	$('.hilight1').hilight();	
	$('.hilight2').hilight({
		background: 'black',
		onFormatted : function(){
			console.info($(this).html());
		}
	});
		
	var callback = function(resultado){
		var $this = $(this);
		var $submit = $this.find('input[type="submit"]');
		//var $submit = $this.find(':submit');
		
		var json = $.parseJSON(resultado);
		
		$submit.val(json.text);
	};
	
	$('form').basicValidation().ajaxForm({
		onSuccess : callback
	});

});