(function($){
		
	/** FONTSIZER **/
	$.fn.fontSizer = function(options)	{
		// Defaults privados
		var defaults = { target : ".fontsizer-target", onFontResized : function(){} };
		var settings = $.extend({}, defaults, options);
	
		return this.each(function(){
			var $this = $(this);
			
			$this.on('click', function(){
				/*$(settings.target)
					.animate({'font-size': $this.data('fontsizer')}).promise()
					.done(function(){
						settings.onFontResized.call($(settings.target));
					});	*/

				$.when($(settings.target).animate({'font-size': $this.data('fontsizer')}))
					.done(function(){
						settings.onFontResized.call($(settings.target))
					});
			});
		});			
	}
	
	/** HILIGHT **/
	$.fn.hilight  = function(options){
	
		var settings = $.extend({}, $.fn.hilight.defaults, options);
	
		return this.each(function(){
			var $this = $(this);
			$this.css('color', settings.foreground);
			$this.css('background-color', settings.background);
			
			var originalHtml = $this.html();
			var formattedHtml = $.fn.hilight.format(originalHtml);
			$this.html(formattedHtml);
			
			settings.onFormatted.call(this);
		});
	};
	
	// Defaults públicos
	$.fn.hilight.defaults = {
		foreground: "red",
		background: "yellow",
		onFormatted : function() {}
	};
	
	$.fn.hilight.format = function( txt ) {
		return "<strong>" + txt + "</strong>";
	};
	
	
	/** AJAX FORM **/	
	$.fn.ajaxForm = function(options) {
		var settings = $.extend({}, $.fn.ajaxForm.defaults, options);
		
		return this.each(function(){
			var $this = $(this);
			$this.on('submit', function(ev) {
				ev.preventDefault();
				
				if($this.data('basicValidation-isvalid'))
				{
					var $submit = $this.find(':submit');
					$submit.prop('disabled', true);
					var data = $this.serialize();
					
					$.post($this.attr('action'), data)
						.done(function(res){
							console.info('success');
							settings.onSuccess.call($this, res);
						})
						.fail(function(){
							console.info('fail!');
						})
						.always(function(){
							$submit.prop('disabled', false);
						});
				}
			});
		});
	};
	
	$.fn.ajaxForm.defaults = {
		onSuccess : function(){}
	}
	
	/* VALIDATOR */
	$.fn.basicValidation = function() {
		
		return this.each(function(){
			var $this = $(this);
			$this.on('submit', function(ev) {				
				var isValid = true;
				
				var validateTargets = $this.find('.basic-validation');
				
				$.each(validateTargets, function(i, item){
					var $target = $(item);
					
					if($target.data('basic-validation-required'))
					{
						if(!$target.val())
						{
							isValid = false;
							$target.addClass('basic-validation-error');
						}
						else
						{
							$target.removeClass('basic-validation-error');
						}
					}
				});
				
				$this.data('basicValidation-isvalid', isValid);
				if(!isValid)
				{
					ev.preventDefault();					
				}
			});
		});	
	}
})(jQuery);