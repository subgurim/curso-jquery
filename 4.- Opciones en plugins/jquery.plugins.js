(function($){
	$.fn.fontSizer = function(options){
		var defaults = { 
			target : ".fontsizer-target", 
			onFontResized : function(){} 
		};
		
		var settings = $.extend({}, defaults, options);
	
		return this.each(function(){
			var $this = $(this);
			
			$this.on('click', function(){
				var $this = $(this);
				var fontSize = $this.data('fontsizer');
				
				$.when($(settings.target).animate({'font-size' : fontSize}))
					.done(function(){
						settings.onFontResized.call($(settings.target), 'p1', 'p2', 'p3');
					});
				
			});
		});
	};
	
	$.fn.hilight = function(options){
		
		var settings = $.extend({}, $.fn.hilight.defaults, options);
		
		return this.each(function(){
			var $this = $(this);
			
			$this.css({
				'color': settings.foreground, 
				'background-color': settings.background				
			});
			
			var originalHtml = $this.html();
			var formattedHtml = "<strong>" + originalHtml +"</strong>";
			
			$this.html(formattedHtml);
			
			settings.onFormatted.call(this);
		});
	};
	
	$.fn.hilight.defaults = {
		foreground : "red",
		background : "yellow",
		onFormatted : function(){}
	};
	
	
	$.fn.ajaxForm = function(options){
		var settings = $.extend({}, $.fn.ajaxForm.defaults, options);
	
		return this.each(function(){
			var $this = $(this);
			var self = this;
			
			$this.on('submit', function(ev){
				ev.preventDefault();
				
				if($this.data('basicvalidation-isvalid'))
				{
					var $submit = $this.find(':submit');
					$submit.prop('disabled', true);
					
					var data = $this.serialize();
					
					$.post($(this).attr('action'), data)
						.done(function(resultado){
							console.info('success');
							settings.onSuccess.call(self, resultado);
						})
						.fail(function(resultado){
							console.info('fail');
							settings.onFail.call(self, resultado);
						})
						.always(function(){
							$submit.prop('disabled', false);
						});
				}
			});
		});
	};
	
	$.fn.ajaxForm.defaults = {
		onSuccess : function(){},
		onFail : function(){}
	};
	
	$.fn.basicValidation = function(){
		
		return this.each(function(){
			var $this = $(this);
			
			$this.on('submit', function(ev){
				ev.preventDefault();
				
				var isValid = true;

				var validateTargets = $this.find('.basic-validation');
				
				$.each(validateTargets, function(i, item){
					var $target = $(item);
					
					isValid = $target.val() != '';			
					$target.toggleClass('basic-validation-error', !isValid);
				});
				
				$this.data('basicvalidation-isvalid', isValid);
			});			
		});
	};
	
})(jQuery);