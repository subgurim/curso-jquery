/**
 * A jQuery plugin boilerplate.
 * Author: Jonathan Nicol @f6design
 */
;(function($) {
  var pluginName = 'advancedValidation';

  function Plugin(element, options) {
    var el = element;
    var $el = $(element);

    options = $.extend({}, $.fn[pluginName].defaults, options);

    function init() {
      
	  $el.on('submit', function(ev){
		validate().fail(function(){
			ev.preventDefault();
		});
	  });
	  
	  
      hook('onInit');
    }
	
	function validate() {
		return $.when(_validate(), hook('onValidating'))
			.done(function(){ hook('onIsValid'); })
			.fail(function(){ hook('onIsNotValid'); } )
			.always(function(){ hook('onValidated'); });
	}
	
	function _validate() {
		var deferred = $.Deferred();
		
		setIsValid(true);
		
		var targetValidation = $('.advanced-validation');
		$.each(targetValidation, function(i, item){
			var $target = $(item);
			var targetValue = $target.val();
			var isTargetValid = true;
			
			// ¿es requerido?
			if($target.data('advanced-validation-required')){
				var isValid = targetValue != '';
				if(!isValid){
					setIsValid(false);
					isTargetValid = false;
				}
				else {
					isTargetValid = true;
				}	
			}
			
			if(isTargetValid)
			{
				// ¿validación regex?
				var regexValidation = $target.data('advanced-validation-regex');
				if(regexValidation) {
					var reg = new RegExp(regexValidation);
					var isValid = reg.test(targetValue);
					
					if(!isValid){
						setIsValid(false);
						isTargetValid = false;
					}
					else {
						isTargetValid = true;
					}
				}
			}
			
			setIsTargetValid(isTargetValid);
			function setIsTargetValid(isValid) {
				$target.toggleClass('advanced-validation-error', !isValid);
			}
		});
		
		if(isValid()) {
			deferred.resolve();
		}
		else {
			deferred.reject();
		}			
	
		return deferred.promise();
	}
	
	function setIsValid(isValid, $target) {
		$el.data('advanced-validation-isvalid', isValid);
	}	

    function isValid() {
		return $el.data('advanced-validation-isvalid');
    }

    function option (key, val) {
      if (val) {
        options[key] = val;
      } else {
        return options[key];
      }
    }

    function destroy() {
      $el.each(function() {
        var el = this;
        var $el = $(this);

        // Add code to restore the element to its original state...
        hook('onDestroy');
        $el.removeData('plugin_' + pluginName);
      });
    }

    function hook(hookName) {
      if (options[hookName] !== undefined) {
        options[hookName].call(el);
      }
    }

    init();

    return {
      option: option,
      destroy: destroy,
      isValid: isValid,
	  validate: validate
    };
  }

  $.fn[pluginName] = function(options) {
    if (typeof arguments[0] === 'string') {
      var methodName = arguments[0];
      var args = Array.prototype.slice.call(arguments, 1);
      var returnVal;
      this.each(function() {
        if ($.data(this, 'plugin_' + pluginName) && typeof $.data(this, 'plugin_' + pluginName)[methodName] === 'function') {
          returnVal = $.data(this, 'plugin_' + pluginName)[methodName].apply(this, args);
        } else {
          throw new Error('Method ' +  methodName + ' does not exist on jQuery.' + pluginName);
        }
      });
      if (returnVal !== undefined){
        return returnVal;
      } else {
        return this;
      }
    } else if (typeof options === "object" || !options) {
      return this.each(function() {
        if (!$.data(this, 'plugin_' + pluginName)) {
          $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
        }
      });
    }
  };

  $.fn[pluginName].defaults = {
    onInit: function() {},
    onDestroy: function() {}
  };

})(jQuery);