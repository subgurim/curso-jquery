$(document).ready(function(){	
	$('form').advancedValidation({
		onIsValid : function() {
			console.log('Is Valid');
		},
		onIsNotValid : function() {
			console.log('Is Not Valid');
		}
	});
	
	$('#validate').on('click', function(ev){
		ev.preventDefault();
		$('form').advancedValidation('validate');
	});
});