Handlebars.registerHelper('toh1', function(text){
	text = Handlebars.Utils.escapeExpression(text);
	
	return new Handlebars.SafeString(
		"<h1>" + text + "</h1>"
	);
});

Handlebars.registerHelper('pablo', function(text){
	var x =  Handlebars.Utils.escapeExpression("pabló&" + text);
	return new Handlebars.SafeString(
		x
	);
	
	//return "pabló&" + text;
});

Handlebars.registerHelper('francisco', function(items, options){
  var out = "<ul>";

  for(var i=0; i<items.length; i++) {
    out = out + "<li>" + options.fn(items[i]) + "</li>";
  }

  return out + "</ul>";
});