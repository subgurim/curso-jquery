/*var NS1 = NS1 || {};
NS1.NS2 = NS1.NS2 || {};

NS1.NS2.ClassName = (function($) {
	var methods = {};
	
	var privateMethod = function(){
	};
	
	methods.PublicMethod = function (){
		privateFunction();
	};
	
	return methods;
})(jQuery)

$(document).ready(function(){
	NS1.NS2.ClassName.PublicMethod();
});*/


$(document).ready(function(){
	$('.urlize').urlize().underlinize();
	$('.boldize').boldize();
	$('.colorize').colorize();
	
	$('.font-sizer').fontSizer();
	
	$('#template-target').templatize(
		'template', 
		{ 
			title: 'My title', 
			body : 'My body'
		}
		//'json.aspx'
	);
});