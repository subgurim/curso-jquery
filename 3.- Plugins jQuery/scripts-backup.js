$(document).ready(function(){
	$('.urlize').urlize();
	//$('.urlize').urlize().underlinize();
	$('.boldize, .colorize').boldize().colorize();
	$('.font-sizer').fontSizer();
	
	$('#template-target').templatize('template', { title : 'My title', body : 'My body'});
	//$('#template-target').templatize2('template', 'json.aspx');
});