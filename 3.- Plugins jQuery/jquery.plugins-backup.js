// Ejemplos sin usar "properties" que se dar�n en la siguiente clase

(function($){
	$.fn.urlize = function (){
		$(this).on('click', function(){
			location.href = $(this).data('url');
		});
	};
	
	$.fn.urlize2 = function (){
		return this.each(function(){
			var $this = $(this);
			$this.css('cursor', 'pointer');
			
			$this.on('click', function(){
				var url = $this.data('url');
				if(url)	{
					location.href = url;
				}
			})

		});
	};
	
	$.fn.boldize = function (){
		return this.each(function(){
			var $this = $(this);
			$this.css('font-weight', 'bold');
		});
	};
	
	$.fn.underlinize = function (){
		return this.each(function(){
			var $this = $(this);
			$this.css('text-decoration', 'underline');
		});
	};
	
	$.fn.colorize = function(){
		return this.each(function(){
			var $this = $(this);
			var color = $this.data('color');
			if(color) {
				$this.css('color', color);
			}
		});
	};
	
	$.fn.fontSizer = function()	{
		return this.each(function(){
			var $this = $(this);
			
			$this.on('click', function(){
				$('#mydiv').animate({'font-size': $this.data('fontsizer')});
			});
		});
	};
	
	$.fn.templatize = function(templateId, data) {
		return this.each(function(){
			var $this = $(this);
			
			var source = $('#' + templateId).html();
			var template = Handlebars.compile(source);
			var html = template(data)
			$this.html(html);
		});
	};
	
	$.fn.templatize2 = function(templateId, data) {
		return this.each(function(){
			var $this = $(this);
			
			var source = $('#' + templateId).html();
			var template = Handlebars.compile(source);
			var html;

			if(typeof data == 'string')	{
				$.getJSON(data).done(function(json){
					write(json);
				});
			}
			else {
				write(data);
			}
			
			function write(data) {
				html = template(data)			
				$this.html(html);
			}
		});
	};
})(jQuery);