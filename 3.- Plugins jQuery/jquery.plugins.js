(function($){
	$.fn.urlize = function(){	
		var $this = $(this);
		$this.css('cursor', 'pointer');	

		return this.each(function(){
			var $this = $(this);
			$this.on('click', function(){
				var $this = $(this);
				var url = $this.data('url');
				location.href= url;
			});	
		});	
	};
	
	$.fn.boldize = function(){
		return this.each(function(){
			var $this = $(this);
			$this.css('font-weight', 'bold');
		})
	};
	
	$.fn.underlinize = function(){
		return this.each(function(){
			var $this= $(this);
			$this.css('text-decoration','underline');
		});
	};
	
	$.fn.colorize = function(){
		return this.each(function(){
			var $this = $(this);
			var color = $this.data('color');
			
			if(color){
				$this.css('color', color);
			}
		});
	}
	
	$.fn.fontSizer = function(){
		return this.each(function(){
			var $this = $(this);
			
			$this.on('click', function(){
				var $this = $(this);
				var fontSize = $this.data('fontsizer');
				
				$('#mydiv').animate({'font-size' : fontSize});
			});
		});
	}
	
	$.fn.templatize = function(templateId){
		var source = $('#' + templateId).html();
		var template = Handlebars.compile(source);
	
		return this.each(function(){
			var $this = $(this);
			var html;
			
			var dataOrUrl = $this.data('dataorurl');
			
			if(typeof dataOrUrl == 'string') {
				$.getJSON(dataOrUrl).done(function(json){
					write(json);
				});
			}
			else {
				write(dataOrUrl);
			}
			
			function write(data){
				html = template(data);
				$this.html(html);
			}
		});
	}
	
})(jQuery)