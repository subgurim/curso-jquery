$(sortable);

function sortable() {
	$( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
}

function resizable() {
	$('.resizable').resizable();
	//$('.resizable').resizable().draggable();
	/*$('.resizable').resizable({
      animate: true,
	  helper: "ui-resizable-helper"
    });*/
}


function droppableExamples(){
	$( "#draggable" ).draggable();
	$( "#draggable-not-valid" ).draggable();
	
	var $droppable = $( "#droppable" );
    $droppable.droppable({
      drop: function( event, ui ) {
        $( this )
          .addClass( "ui-state-highlight" )
          .find( "p" )
            .html( "Dropped!" );
      }
    });
	
	$droppable.droppable('option', 'accept', '#draggable');
}
  
 function draggableExamples(){
	// Draggable simple
    //$( "#draggable" ).draggable();
	
	// Constrain movement
    /*$( "#draggable" ).draggable({ axis: "y" });
    $( "#draggable2" ).draggable({ axis: "x" });
 
    $( "#draggable3" ).draggable({ containment: "#containment-wrapper", scroll: false });
    $( "#draggable5" ).draggable({ containment: "parent" });*/
	
	// Events
	var startCount = 0;
	var stopCount = 0;
	var $panel = $( "#draggable" );
	
	$panel.draggable({
		start : function() {
			startCount++;
			console.log('#start: ' + startCount);
		}
	});
	
	// Añadir opciones posteriormente
	$panel.draggable('option', 'stop', function(){
		stopCount++;
		console.log('#stop: ' + stopCount);
	});
 }